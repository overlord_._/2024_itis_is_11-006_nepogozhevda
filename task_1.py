import re
import requests

from bs4 import BeautifulSoup
from urllib.parse import urljoin, unquote

from config import (
    INDEX_TXT_PATH,
    MAIN_URLS,
    MIN_LINKS_AMOUNT,
    MIN_WORDS_COUNT,
    PAGE_REQUEST_TIMEOUT,
    PAGE_STOR,
)


CORRECT_LINKS = []
ALL_SEEN_LINKS = set()
LINKS_TO_CRAWL = []


def get_page(link):
    try:
        r = requests.get(link, timeout=PAGE_REQUEST_TIMEOUT)
    except requests.exceptions.ConnectionError:
        return None
    return BeautifulSoup(r.text, "html.parser")


def extract_links(link, soup):
    for a_tag in soup.find_all("a"):
        link = urljoin(link, a_tag.get("href"))
        if link not in ALL_SEEN_LINKS:
            LINKS_TO_CRAWL.append(link)
            ALL_SEEN_LINKS.add(link)


def extract_text(soup):
    text = soup.get_text()
    p = re.compile(r'<.*?>')
    text = p.sub(' ', text)
    text = re.sub(r'\n+', '\n', text)
    text = re.sub(r'([^\nа-яА-ЯёЁ-])', ' ', text)
    text = re.sub(' +', ' ', text)
    text = re.sub(r'\n\s*\n', '\n', text)
    text = re.sub(r'\n ', '\n', text)
    return text


def count_words(text):
    words = re.split(r"\s+|\t|\n", text)[1:-1]
    while "-" in words:
        words.remove("-")
    return len(words)


def process_page(link):
    soup = get_page(link)
    if not soup:
        return None
    extract_links(link, soup)
    text = extract_text(soup)
    words_cnt = count_words(text)
    return words_cnt, text


if __name__ == "__main__":
    for main_url in MAIN_URLS:
        if len(CORRECT_LINKS) >= MIN_LINKS_AMOUNT:
            break
        
        ALL_SEEN_LINKS.add(main_url)
        LINKS_TO_CRAWL.append(main_url)

        while len(CORRECT_LINKS) < MIN_LINKS_AMOUNT and len(LINKS_TO_CRAWL) > 0:
            current_link = LINKS_TO_CRAWL.pop(0)
            process_result = process_page(current_link)
            if not process_result:
                continue
            if process_result[0] < MIN_WORDS_COUNT:
                continue
            current_page_number = len(CORRECT_LINKS) + 1
            CORRECT_LINKS.append(current_link)

            with open(INDEX_TXT_PATH, "a", encoding="utf-8") as file:
                file.write(
                    f"{current_page_number};{unquote(current_link)};{process_result[0]}\n"
                )
            with open(
                f"{PAGE_STOR}/{current_page_number}.txt", "w", encoding="utf-8"
            ) as file:
                file.write(process_result[1])
